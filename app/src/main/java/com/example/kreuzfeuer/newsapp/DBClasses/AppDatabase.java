package com.example.kreuzfeuer.newsapp.DBClasses;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.kreuzfeuer.newsapp.NewsItem;

/**
 * Created by Kreuzfeuer on 09.10.2017.
 */

@Database(entities = {NewsItem.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsDao newsDao();
}