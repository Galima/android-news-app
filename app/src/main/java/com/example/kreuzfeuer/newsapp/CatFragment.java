package com.example.kreuzfeuer.newsapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Kreuzfeuer on 09.10.2017.
 */


public class CatFragment extends Fragment {

    private RecyclerView recyclerView;
    private CatAdapter adapter;
    private List<Cat> categoryList;

    public CatFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.cats_fragment, container, false);
        // Inflate the layout for this fragment

        recyclerView = view.findViewById(R.id.recycler_view);

        categoryList = getCategories();
        adapter = new CatAdapter(this.getContext(), categoryList);
        recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        return view;
    }

    private List<Cat> getCategories() {
        List<Cat> catList = new ArrayList<Cat>();

        catList.add(new Cat("News & Politics", R.drawable.cat1));
        catList.add(new Cat("Sports", R.drawable.cat2));
        catList.add(new Cat("Travel", R.drawable.cat3));
        catList.add(new Cat("Health", R.drawable.cat4));
        catList.add(new Cat("Science", R.drawable.cat5));
        catList.add(new Cat("Business", R.drawable.cat6 ));

        return catList;
    }
}