package com.example.kreuzfeuer.newsapp.DBClasses;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.kreuzfeuer.newsapp.NewsItem;

import java.util.List;

/**
 * Created by Kreuzfeuer on 09.10.2017.
 */

@Dao
public interface NewsDao {

    @Query("SELECT * FROM news")
    List<NewsItem> getAll();

    @Insert
    void insert(NewsItem news);

    @Delete
    void delete(NewsItem news);

    @Query("SELECT * FROM news")
    List<NewsItem> getAllNews();
}
