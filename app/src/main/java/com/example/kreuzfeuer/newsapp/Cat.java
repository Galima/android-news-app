package com.example.kreuzfeuer.newsapp;

/**
 * Created by Kreuzfeuer on 09.10.2017.
 */

public class Cat {
    private String title;
    private int picture;

    public Cat(){}

    public Cat(String title, int picture) {
        this.title = title;
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }
}