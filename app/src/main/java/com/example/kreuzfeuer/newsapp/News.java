package com.example.kreuzfeuer.newsapp;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created by Kreuzfeuer on 01.10.2017.
 */

public class News implements Parcelable {

    String name;
    String date;
    String description;

    public News(){

    }
    protected News(Parcel in) {
        name = in.readString();
        date = in.readString();
        description = in.readString();
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(date);
        dest.writeString(description);
    }
}


