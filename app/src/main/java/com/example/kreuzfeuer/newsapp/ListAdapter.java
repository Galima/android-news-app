package com.example.kreuzfeuer.newsapp;

/**
 * Created by Kreuzfeuer on 01.10.2017.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Context mContext;
    List<News> mNewsList;

    private static final int FIRST = 1;
    private static final int SECOND = 2;

    public ListAdapter(Context context, List<News> newsList) {
        mContext = context;
        mNewsList = newsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("ViewHolder", "Create");
        View view;
        if (viewType == FIRST) {
            view = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
            return new MyViewHolder(view);
        }

        view = LayoutInflater.from(mContext).inflate(R.layout.new_list_item, parent, false);

        return new SecondViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.e("ViewHolder", "Bind");

        if (getItemViewType(position) == FIRST) {
            MyViewHolder viewHolder = (MyViewHolder) holder;
            News contact = mNewsList.get(position);
            viewHolder.setPosition(position);
            viewHolder.mName.setText(contact.getName());
            viewHolder.mDate.setText(contact.getDate());
        }
    }

    @Override
    public int getItemCount() {
        return mNewsList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1)
            return SECOND;

        return FIRST;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mName;
        TextView mDate;
        int position;

        public MyViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.name);
            mDate = (TextView) itemView.findViewById(R.id.date);

            itemView.setOnClickListener(this);
        }


        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, NewsDetailsActivity.class);
            intent.putExtra("news", mNewsList.get(position));
            mContext.startActivity(intent);
        }


    }


    class SecondViewHolder extends RecyclerView.ViewHolder {


        public SecondViewHolder(View itemView) {
            super(itemView);
        }
    }
}
