package com.example.kreuzfeuer.newsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    List<News> newsList = new ArrayList<>();
    ListAdapter listAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

//        List<News> newsList = new ArrayList<>();
        for (int i=0; i<20; i++){
            News news = new News();
            news.setName("Name "+i);
            news.setDate("8777 777 77 7"+i);
            newsList.add(news);
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        listAdapter = new ListAdapter(this, newsList);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
//        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
    }


    public void click(View v){
       // for (int i=3; i<6; i++){

//            News news = new News();
//            news.setName("Name "+2222);
//            news.setDate("Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world");
            Intent intent = new Intent(this, AddNewsActivity.class);
            intent.putParcelableArrayListExtra("newsList", (ArrayList<News>) newsList);
            startActivity(intent);
            //newsList.add(news);
        //}

        listAdapter.notifyItemInserted(1);
        newsList.remove(0);
        listAdapter.notifyItemRemoved(0);
    }

}



