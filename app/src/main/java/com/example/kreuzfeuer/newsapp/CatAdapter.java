package com.example.kreuzfeuer.newsapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;


import java.util.List;
/**
 * Created by Kreuzfeuer on 09.10.2017.
 */

public class CatAdapter extends RecyclerView.Adapter<CatAdapter.MyViewHolder> {
    private Context mContext;
    private List<Cat> categoryList;

    public CatAdapter(Context _mContext, List<Cat> _categoryList) {
        this.mContext = _mContext;
        this.categoryList = _categoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cat_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Cat album = categoryList.get(position);
        holder.title.setText(album.getTitle());

        // loading category cover using Glide library
        Glide.with(mContext).load(album.getPicture()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    //View Holder class
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }
}