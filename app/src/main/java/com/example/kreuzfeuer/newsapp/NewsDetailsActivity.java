package com.example.kreuzfeuer.newsapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class NewsDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        News news = getIntent().getParcelableExtra("news");

        TextView textView = (TextView) findViewById(R.id.name);
        TextView textView1 = (TextView) findViewById(R.id.date);

        textView.setText(news.getName());
        textView1.setText(news.getDate());
    }
}
